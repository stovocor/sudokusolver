package de.freewarepoint.sudoku

import kotlin.collections.ArrayList

class Block (private val board: SudokuBoard) {

    val topLine: List<Field> = listOf(Field(this), Field(this), Field(this))
    val midLine: List<Field> = listOf(Field(this), Field(this), Field(this))
    val bottomLine: List<Field> = listOf(Field(this), Field(this), Field(this))

    fun completeTopLine(): List<Field> {
        val completeLine = mutableListOf<Field>()

        val left = board.leftOf(this)
        left?.let { completeLine.addAll(left.leftTopLine()) }

        completeLine.addAll(topLine)

        val right = board.rightOf(this)
        right?.let { completeLine.addAll(right.rightTopLine()) }

        return completeLine
    }

    private fun leftTopLine(): List<Field> {
        val leftLine = mutableListOf<Field>()

        val left = board.leftOf(this)
        if (left != null) {
            leftLine.addAll(left.leftTopLine())
        }

        leftLine.addAll(topLine)

        return leftLine
    }

    private fun rightTopLine(): List<Field> {
        val rightLine = mutableListOf<Field>()

        rightLine.addAll(topLine)

        val right = board.rightOf(this)
        if (right != null) {
            rightLine.addAll(right.rightTopLine())
        }

        return rightLine
    }

    fun upperField(field: Field): Field? {
        val containingLine: List<Field> = containingLine(field)
        val horizontalPosition = containingLine.indexOf(field)

        return when (containingLine) {
            topLine -> {
                val upper = board.topOf(this) ?: return null
                upper.bottomLine[horizontalPosition]
            }
            midLine -> topLine[horizontalPosition]
            else -> midLine[horizontalPosition]
        }
    }

    fun rightField(field: Field): Field? {
        val containingLine: List<Field> = containingLine(field)

        return if (containingLine.indexOf(field) < containingLine.size - 1) {
            containingLine[containingLine.indexOf(field) + 1]
        } else {
            val right = board.rightOf(this) ?: return null;

            when (containingLine) {
                topLine -> right.topLine[0]
                midLine -> right.midLine[0]
                else -> right.bottomLine[0]
            }
        }
    }

    fun lowerField(field: Field): Field? {
        val containingLine: List<Field> = containingLine(field)
        val horizontalPosition = containingLine.indexOf(field);

        return when (containingLine) {
            topLine -> midLine[horizontalPosition]
            midLine -> bottomLine[horizontalPosition]
            else -> {
                val bottom = board.belowOf(this) ?: return null;
                bottom.topLine[horizontalPosition];
            }
        }
    }

    fun leftField(field: Field): Field? {
        val containingLine: List<Field> = containingLine(field)

        return if (containingLine.indexOf(field) > 0) {
            containingLine[containingLine.indexOf(field) - 1]
        } else {
            val left = board.leftOf(this) ?: return null;

            when (containingLine) {
                topLine -> left.topLine[left.topLine.size - 1]
                midLine -> left.midLine[left.midLine.size - 1]
                else -> left.bottomLine[left.bottomLine.size - 1]
            }
        }
    }


    private fun containingLine(field: Field): List<Field> {
        return when {
            topLine.contains(field) -> topLine
            midLine.contains(field) -> midLine
            bottomLine.contains(field) -> bottomLine
            else -> throw IllegalStateException("Field not found in block")
        }
    }

    fun allFields(): List<Field> {
        return listOf(topLine[0], topLine[1], topLine[2],
                midLine[0], midLine[1], midLine[2],
                bottomLine[0], bottomLine[1], bottomLine[2])
    }

    fun isSolved(): Boolean {
        return allFields().all { it.hasKnownValue() }
    }


}