package de.freewarepoint.sudoku

class ClassicSudoku: SudokuBoard {

    private val northWest = Block(this)
    private val north = Block(this)
    private val northEast = Block(this)
    private val west = Block(this)
    private val mid = Block(this)
    private val east = Block(this)
    private val southWest = Block(this)
    private val south = Block(this)
    private val southEast = Block(this)

    override fun initField(blockNumber: Int, fieldNumber: Int, value: Int) {
        val block = allBlocks()[blockNumber]
        val field = block.allFields()[fieldNumber]
        field.initWith(value)
    }

    override fun topOf(block: Block): Block? {
        return when (block) {
            west -> northWest
            mid -> north
            east -> northEast
            southWest -> west
            south -> mid
            southEast -> east
            else -> null
        }
    }

    override fun rightOf(block: Block) : Block? {
        return when (block) {
            northWest -> north
            north -> northEast
            west -> mid
            mid -> east
            southWest -> south
            south -> southEast
            else -> null
        }
    }

    override fun belowOf(block: Block): Block? {
        return when (block) {
            northWest -> west
            north -> mid
            northEast -> east
            west -> southWest
            mid -> south
            east -> southEast
            else -> null
        }
    }


    override fun leftOf(block: Block): Block? {
        return when (block) {
            north -> northWest
            northEast -> north
            mid -> west
            east -> mid
            south -> southWest
            southEast -> south
            else -> null
        }
    }

    override fun allBlocks(): List<Block> {
        return listOf(northWest, north, northEast, west, mid, east, southWest, south, southEast)
    }

    override fun solve() {
        while (!isSolved()) {
            if (!solveNextField()) {
                throw IllegalStateException("No further solvable fields found!")
            }
        }
    }

    private fun solveNextField(): Boolean {
        for (block in allBlocks()) {
            for (field in block.allFields()) {
                if (field.hasKnownValue()) {
                    continue
                }
                val possibleValue = field.possibleValue()
                if (possibleValue != null) {
                    field.setValue(possibleValue)
                    return true
                }
            }
        }

        return false
    }

    override fun isSolved(): Boolean {
        return allBlocks().all { it.isSolved() }
    }

    override fun printableLayout(): String {
        val sb = StringBuilder()
        sb.append("/-----------\\\n")
        
        sb.append("|")
        sb.append(renderLine(northWest.topLine))
        sb.append("|")
        sb.append(renderLine(north.topLine))
        sb.append("|")
        sb.append(renderLine(northEast.topLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(northWest.midLine))
        sb.append("|")
        sb.append(renderLine(north.midLine))
        sb.append("|")
        sb.append(renderLine(northEast.midLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(northWest.bottomLine))
        sb.append("|")
        sb.append(renderLine(north.bottomLine))
        sb.append("|")
        sb.append(renderLine(northEast.bottomLine))
        sb.append("|\n")
        
        sb.append("|-----------|\n")

        sb.append("|")
        sb.append(renderLine(west.topLine))
        sb.append("|")
        sb.append(renderLine(mid.topLine))
        sb.append("|")
        sb.append(renderLine(east.topLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(west.midLine))
        sb.append("|")
        sb.append(renderLine(mid.midLine))
        sb.append("|")
        sb.append(renderLine(east.midLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(west.bottomLine))
        sb.append("|")
        sb.append(renderLine(mid.bottomLine))
        sb.append("|")
        sb.append(renderLine(east.bottomLine))
        sb.append("|\n")

        sb.append("|-----------|\n")

        sb.append("|")
        sb.append(renderLine(southWest.topLine))
        sb.append("|")
        sb.append(renderLine(south.topLine))
        sb.append("|")
        sb.append(renderLine(southEast.topLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(southWest.midLine))
        sb.append("|")
        sb.append(renderLine(south.midLine))
        sb.append("|")
        sb.append(renderLine(southEast.midLine))
        sb.append("|\n")
        sb.append("|")
        sb.append(renderLine(southWest.bottomLine))
        sb.append("|")
        sb.append(renderLine(south.bottomLine))
        sb.append("|")
        sb.append(renderLine(southEast.bottomLine))
        sb.append("|\n")

        sb.append("\\-----------/")
        return sb.toString()
    }

    private fun renderLine(line: List<Field>) : String {
        return line[0].printableLayout() + line[1].printableLayout() + line[2].printableLayout()
    }


}