package de.freewarepoint.sudoku

class Field(private val containedInBlock: Block) {

    var predefined: Boolean = false
    private var number: Int? = null

    fun hasKnownValue(): Boolean {
        return number != null
    }

    fun setValue(value: Int) {
        number = value
    }

    fun printableLayout(): String {
        return if (number != null) {
            number.toString()
        } else {
            " "
        }
    }

    fun initWith(initialValue: Int) {
        number = initialValue
        predefined = true
    }

    fun possibleValue(): Int? {
        if (hasKnownValue()) {
            return number
        }

        val possibleValues = mutableListOf(1, 2, 3, 4, 5, 6, 7, 8, 9)

        // Search in column above
        var nextUpper = containedInBlock.upperField(this)
        while (nextUpper != null) {
            nextUpper.number?.let { possibleValues.remove(it) }
            nextUpper = nextUpper.containedInBlock.upperField(nextUpper)
        }

        // Search in line to the right
        var nextRight = containedInBlock.rightField(this)
        while (nextRight != null) {
            nextRight.number?.let { possibleValues.remove(it) }
            nextRight = nextRight.containedInBlock.rightField(nextRight)
        }

        // Search in column below
        var nextBelow = containedInBlock.lowerField(this)
        while (nextBelow != null) {
            nextBelow.number?.let { possibleValues.remove(it) }
            nextBelow = nextBelow.containedInBlock.lowerField(nextBelow)
        }

        // Search in line to the left
        var nextLeft = containedInBlock.leftField(this)
        while (nextLeft != null) {
            nextLeft.number?.let { possibleValues.remove(it) }
            nextLeft = nextLeft.containedInBlock.leftField(nextLeft)
        }

        // Search in same block
        for (fieldInSameBlock in containedInBlock.allFields()) {
            fieldInSameBlock.number?.let { possibleValues.remove(it) }
        }

        return when {
            possibleValues.size == 0 -> {
                throw IllegalStateException("Invalid solution!")
            }
            possibleValues.size > 1 -> {
                null
            }
            else -> {
                possibleValues[0]
            }
        }
    }

}