package de.freewarepoint.sudoku

interface SudokuBoard {

    fun initField(blockNumber: Int, fieldNumber: Int, value: Int)

    fun leftOf(block: Block) : Block?
    fun rightOf(block: Block) : Block?
    fun topOf(block: Block): Block?
    fun belowOf(block: Block): Block?

    fun allBlocks() : Collection<Block>

    fun printableLayout(): String

    fun solve()

    fun isSolved(): Boolean

}