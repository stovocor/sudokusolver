package de.freewarepoint.sudoku

import org.mockito.Mockito
import kotlin.test.Test

class BlockTest {

    @Test
    fun uninitializedBlock() {
        val block = Block(Mockito.mock(SudokuBoard::class.java))
        assert(block.topLine.size == 3)
        assert(block.midLine.size == 3)
        assert(block.bottomLine.size == 3)
    }

    @Test
    fun topLine() {
        val board = Mockito.mock(SudokuBoard::class.java)

        val left = Block(board)
        val mid = Block(board)
        val right = Block(board)

        Mockito.`when`(board.rightOf(left)).thenReturn(mid)
        Mockito.`when`(board.rightOf(mid)).thenReturn(right)
        Mockito.`when`(board.leftOf(mid)).thenReturn(left)
        Mockito.`when`(board.leftOf(right)).thenReturn(mid)

        assert(left.completeTopLine().size == 9)
    }

    @Test
    fun adjacentFields() {
        val block = Block(Mockito.mock(SudokuBoard::class.java))
        val midField = block.midLine[1]

        assert(block.upperField(midField) == block.topLine[1])
    }

}