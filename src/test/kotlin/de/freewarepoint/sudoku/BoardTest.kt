package de.freewarepoint.sudoku

import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

object BoardSpec: Spek({
    describe("a new board instance") {
        val board = ClassicSudoku()
        it("should be unsolved") {
            assertFalse(board.isSolved())
        }
        it("should only contain unsolved blocks") {
            for (block in board.allBlocks()) {
                assertFalse(block.isSolved())
            }
        }
        it("should have 3x3 blocks") {
            assertEquals(9, board.allBlocks().size)
        }
        it("should have only blocks with unknown values") {
            board.allBlocks()
                    .flatMap { it.allFields() }
                    .forEach { assertFalse(it.hasKnownValue()) }
        }
    }
    describe("a board with one field set") {
        val board = ClassicSudoku()
        board.allBlocks().first().midLine[1].initWith(3)
        it("should have a field marked as predefined") {
            assertFalse(board.allBlocks().first().midLine[0].predefined)
            assertTrue(board.allBlocks().first().midLine[1].predefined)
        }
        it("should be unresolved") {
            assertFalse(board.isSolved())
            for (block in board.allBlocks()) {
                assertFalse(block.isSolved())
            }
        }
    }
})