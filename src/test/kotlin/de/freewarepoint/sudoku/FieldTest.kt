package de.freewarepoint.sudoku

import kotlin.test.Test

class FieldTest {

    @Test
    fun searchInLine() {
        val board = ClassicSudoku()

        board.initField(0, 0, 1)
        board.initField(0, 1, 2)
        board.initField(0, 2, 3)
        board.initField(1, 0, 4)
        board.initField(1, 2, 6)
        board.initField(2, 0, 7)
        board.initField(2, 1, 8)
        board.initField(2, 2, 9)

        val topMid = board.allBlocks()[1]
        val field = topMid.topLine[1]

        assert(!field.hasKnownValue())
        assert(field.possibleValue() == 5)
    }

    @Test
    fun solve() {
        val board = ClassicSudoku()

        // Random board from https://www.websudoku.com/?level=1&set_id=3290794717
        board.initField(0, 0, 4)
        board.initField(0, 2, 5)
        board.initField(0, 6, 6)
        board.initField(0, 7, 8)
        board.initField(0, 8, 1)

        board.initField(1, 1, 8)
        board.initField(1, 3, 4)
        board.initField(1, 6, 7)

        board.initField(2, 2, 1)
        board.initField(2, 4, 6)
        board.initField(2, 5, 2)
        board.initField(2, 8, 4)

        board.initField(3, 1, 6)
        board.initField(3, 2, 2)
        board.initField(3, 4, 9)
        board.initField(3, 7, 4)
        board.initField(3, 8, 7)

        board.initField(4, 1, 5)
        board.initField(4, 7, 3)

        board.initField(5, 0, 3)
        board.initField(5, 1, 8)
        board.initField(5, 4, 4)
        board.initField(5, 6, 1)
        board.initField(5, 7, 9)

        board.initField(6, 0, 7)
        board.initField(6, 3, 8)
        board.initField(6, 4, 1)
        board.initField(6, 6, 2)

        board.initField(7, 2, 3)
        board.initField(7, 5, 9)
        board.initField(7, 7, 4)

        board.initField(8, 0, 4)
        board.initField(8, 1, 1)
        board.initField(8, 2, 8)
        board.initField(8, 6, 7)
        board.initField(8, 8, 9)

        board.solve()
    }

}